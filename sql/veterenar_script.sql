-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema veterenar
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema veterenar
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `veterenar` DEFAULT CHARACTER SET utf8 ;
USE `veterenar` ;

-- -----------------------------------------------------
-- Table `veterenar`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`client` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `visit_date` DATE NOT NULL,
  `phone_number` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `visit_date_UNIQUE` (`visit_date` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`pet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`pet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  `age` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `client_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `client_id`),
  INDEX `fk_pet_client1_idx` (`client_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `veterenar`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`pet_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`pet_type` (
  `type` VARCHAR(45) NOT NULL,
  `breed` VARCHAR(45) NOT NULL,
  UNIQUE INDEX `type_UNIQUE` (`type` ASC) VISIBLE,
  UNIQUE INDEX `breed_UNIQUE` (`breed` ASC) VISIBLE,
  PRIMARY KEY (`type`, `breed`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`pet_medical_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`pet_medical_card` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pet_age` VARCHAR(45) NOT NULL,
  `pet_name` VARCHAR(45) NULL,
  `date_ragistration` DATE NULL,
  `date_last_examination` DATE NULL,
  `client_id` INT UNSIGNED NOT NULL,
  `pet_type_type` VARCHAR(45) NOT NULL,
  `pet_type_breed` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pet_medical_card_client1_idx` (`client_id` ASC) VISIBLE,
  INDEX `fk_pet_medical_card_pet_type1_idx` (`pet_type_type` ASC, `pet_type_breed` ASC) VISIBLE,
  CONSTRAINT `fk_pet_medical_card_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `veterenar`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_medical_card_pet_type1`
    FOREIGN KEY (`pet_type_type` , `pet_type_breed`)
    REFERENCES `veterenar`.`pet_type` (`type` , `breed`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`diagnosis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`diagnosis` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `recomended_medicines` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`vaсcine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`vaсcine` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `vaccine_name` VARCHAR(45) NOT NULL,
  `date_last_injection` DATE NULL,
  `vaсcine_price` DECIMAL NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `vaccine_name_UNIQUE` (`vaccine_name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`pet_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`pet_type` (
  `type` VARCHAR(45) NOT NULL,
  `breed` VARCHAR(45) NOT NULL,
  UNIQUE INDEX `type_UNIQUE` (`type` ASC) VISIBLE,
  UNIQUE INDEX `breed_UNIQUE` (`breed` ASC) VISIBLE,
  PRIMARY KEY (`type`, `breed`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`vaсcine_has_pet_medical_card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`vaсcine_has_pet_medical_card` (
  `vaсcine_id` INT UNSIGNED NOT NULL,
  `pet_medical_card_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`vaсcine_id`, `pet_medical_card_id`),
  INDEX `fk_vaсcine_has_pet_medical_card_pet_medical_card1_idx` (`pet_medical_card_id` ASC) VISIBLE,
  INDEX `fk_vaсcine_has_pet_medical_card_vaсcine1_idx` (`vaсcine_id` ASC) VISIBLE,
  CONSTRAINT `fk_vaсcine_has_pet_medical_card_vaсcine1`
    FOREIGN KEY (`vaсcine_id`)
    REFERENCES `veterenar`.`vaсcine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vaсcine_has_pet_medical_card_pet_medical_card1`
    FOREIGN KEY (`pet_medical_card_id`)
    REFERENCES `veterenar`.`pet_medical_card` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`doctor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`doctor` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NOT NULL,
  `profession` VARCHAR(45) NULL,
  `phone_number` VARCHAR(45) NULL,
  `room` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`visit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`visit` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `doctor_id` INT UNSIGNED NOT NULL,
  `service_id` INT UNSIGNED NOT NULL,
  `visit_date` VARCHAR(45) NULL,
  `diagnosis_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_visit_doctor2_idx` (`doctor_id` ASC) VISIBLE,
  INDEX `fk_visit_service1_idx` (`service_id` ASC) VISIBLE,
  INDEX `fk_visit_diagnosis1_idx` (`diagnosis_id` ASC) VISIBLE,
  CONSTRAINT `fk_visit_doctor2`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `veterenar`.`doctor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `veterenar`.`service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_diagnosis1`
    FOREIGN KEY (`diagnosis_id`)
    REFERENCES `veterenar`.`diagnosis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`days`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`days` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `day_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `day_mae_UNIQUE` (`day_name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`doctor_schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`doctor_schedule` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `days_id` INT UNSIGNED NOT NULL,
  `start_time` TIME NOT NULL,
  `finish_time` TIME NOT NULL,
  `visit_id` INT UNSIGNED NOT NULL,
  `doctor_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_doctor_schedule_visit1_idx` (`visit_id` ASC) VISIBLE,
  INDEX `fk_doctor_schedule_doctor1_idx` (`doctor_id` ASC) VISIBLE,
  INDEX `fk_doctor_schedule_days1_idx` (`days_id` ASC) VISIBLE,
  CONSTRAINT `fk_doctor_schedule_visit1`
    FOREIGN KEY (`visit_id`)
    REFERENCES `veterenar`.`visit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_doctor_schedule_doctor1`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `veterenar`.`doctor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_doctor_schedule_days1`
    FOREIGN KEY (`days_id`)
    REFERENCES `veterenar`.`days` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`visit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`visit` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `doctor_id` INT UNSIGNED NOT NULL,
  `service_id` INT UNSIGNED NOT NULL,
  `visit_date` VARCHAR(45) NULL,
  `diagnosis_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_visit_doctor2_idx` (`doctor_id` ASC) VISIBLE,
  INDEX `fk_visit_service1_idx` (`service_id` ASC) VISIBLE,
  INDEX `fk_visit_diagnosis1_idx` (`diagnosis_id` ASC) VISIBLE,
  CONSTRAINT `fk_visit_doctor2`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `veterenar`.`doctor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `veterenar`.`service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_diagnosis1`
    FOREIGN KEY (`diagnosis_id`)
    REFERENCES `veterenar`.`diagnosis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`service` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` VARCHAR(45) NOT NULL,
  `price` DECIMAL NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `service_name_UNIQUE` (`service_name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`visit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`visit` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `doctor_id` INT UNSIGNED NOT NULL,
  `service_id` INT UNSIGNED NOT NULL,
  `visit_date` VARCHAR(45) NULL,
  `diagnosis_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_visit_doctor2_idx` (`doctor_id` ASC) VISIBLE,
  INDEX `fk_visit_service1_idx` (`service_id` ASC) VISIBLE,
  INDEX `fk_visit_diagnosis1_idx` (`diagnosis_id` ASC) VISIBLE,
  CONSTRAINT `fk_visit_doctor2`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `veterenar`.`doctor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `veterenar`.`service` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_diagnosis1`
    FOREIGN KEY (`diagnosis_id`)
    REFERENCES `veterenar`.`diagnosis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`vaсcine_list`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`vaсcine_list` (
  `vaсcine_id` INT UNSIGNED NOT NULL,
  `pet_medical_card_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`vaсcine_id`, `pet_medical_card_id`),
  INDEX `fk_vaсcine_has_pet_medical_card1_pet_medical_card1_idx` (`pet_medical_card_id` ASC) VISIBLE,
  INDEX `fk_vaсcine_has_pet_medical_card1_vaсcine1_idx` (`vaсcine_id` ASC) VISIBLE,
  CONSTRAINT `fk_vaсcine_has_pet_medical_card1_vaсcine1`
    FOREIGN KEY (`vaсcine_id`)
    REFERENCES `veterenar`.`vaсcine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vaсcine_has_pet_medical_card1_pet_medical_card1`
    FOREIGN KEY (`pet_medical_card_id`)
    REFERENCES `veterenar`.`pet_medical_card` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`visit_list`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`visit_list` (
  `pet_medical_card_id` INT UNSIGNED NOT NULL,
  `visit_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`pet_medical_card_id`, `visit_id`),
  INDEX `fk_pet_medical_card_has_visit_visit1_idx` (`visit_id` ASC) VISIBLE,
  INDEX `fk_pet_medical_card_has_visit_pet_medical_card1_idx` (`pet_medical_card_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_medical_card_has_visit_pet_medical_card1`
    FOREIGN KEY (`pet_medical_card_id`)
    REFERENCES `veterenar`.`pet_medical_card` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_medical_card_has_visit_visit1`
    FOREIGN KEY (`visit_id`)
    REFERENCES `veterenar`.`visit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `veterenar`.`timestamps`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veterenar`.`timestamps` (
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` TIMESTAMP NULL);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
