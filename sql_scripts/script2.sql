use labor_sql;

-- IN ANY ALL

select distinct maker from product
where not maker in(
select maker from product
where type='Laptop')
and type='PC'; 

select maker from product
where maker != all(
select maker from product
where type='Laptop' and type !='PC'
)and type!='Printer';


select distinct maker from product
where not maker = any
(select maker from product
where type='Laptop')
and type='PC';

select distinct maker from product
where maker in
(select maker from product
where type='PC')and type='Laptop';


select distinct maker from product
where not maker != all
(select maker from product
where type='PC')and type='Laptop';

select distinct maker from product
where maker =any
(select maker from product
where type='PC')and type='Laptop';

-- select distinct maker from product
-- inner join pc on product.model = pc.model;

select distinct maker from product
where model in
(select model from pc);

select distinct maker from product
where model = any
(select model from pc);

select distinct maker from product
where not model != all
(select model from pc
where product.model = pc.model);

select country, class from classes
where country='Ukraine' or country in
(select country from classes);

select ship, battle, date, result from outcomes
inner join battles on outcomes.battle = battles.name
where not battles.date <=all
(select date from battles
where outcomes.result = 'damaged');

select count(model) from product
where maker='A' and type='PC' and model in
(select model from pc);

select maker from product
where model !=all
(select model from pc) and type='PC';

select model, price from laptop
where price >all
(select price from pc);


-- ESXISTS
select maker from product
where exists
(select model from pc
where product.model = pc.model);

select maker from product
where exists(
select model from pc 
where product.model = pc.model and speed >= 750
);

select distinct maker from product
where exists(
select pc.model, laptop.model from pc, laptop 
where (product.model = pc.model and pc.speed >= 750) or (product.model = laptop.model and laptop.speed >= 750 )  
);


select maker, pc.model, max(speed) from pc inner join
product on pc.model = product.model
where exists(
select model from product
where pc.model = product.model 
and not product.maker !=all
(select maker from product
where type='Printer')
);

select name, launched, classes.displacement from ships, classes
where launched >= 1922 and exists
(select displacement from classes
where ships.class = classes.class and displacement > 35000);


select class from classes
where exists
(select ship from ships inner join outcomes
on ships.name = outcomes.ship
where classes.class=ships.class and outcomes.result = 'sunk');

#
#

-- Concat

-- select cast(price as nvarchar(10)) from laptop;

select concat('модел: ', model) as model, concat('ціна: ', price) as price from pc;

select date_format(date, "%Y-%m-%d") from income_o;

select 
case result
when 'sunk' then 'потоплений'
when 'damaged' then 'пошкоджений'
when 'OK' then 'цілий'
else 'не вказано'
end as result
from outcomes;

select concat('ряд: ',substring(place, 1, 1))as 'Ряд', 
concat('місце: ',substring(place, 2, 1))as 'Місце'
 from pass_in_trip;
 
 select concat('from ', town_from, ' to ', town_to) as Trip from trip;