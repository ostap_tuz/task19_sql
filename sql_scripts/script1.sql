use labor_sql;

select maker, type from product
order by maker;

select model, ram, screen, price
from laptop
order by ram asc, price desc;

select * from printer
where color = 'y'
order by price desc;

select model, speed, hd, cd, price
from pc
where (cd='12x' or cd='24x') and price < 600
order by speed desc;